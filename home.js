var comingsoon = document.getElementsByClassName('nba')[0];
var comingsoonBox = document.getElementsByClassName('text')[0];
var comingsoonText = document.getElementById('comingSoon');
var sphereLeft = document.getElementsByClassName('sphere-l')[0];
var sphereRight = document.getElementsByClassName('sphere-r')[0];
var panStatus;

panStatus = 0;
comingsoon.addEventListener('click', function (){
    if(panStatus == 0){
        comingsoonBox.style.animation = "panin 2s cubic-bezier(.39,.58,.57,1) 0s 1 forwards";
        comingsoonText.textContent = "Coming Soon ...";
        panStatus = 1;
    }
    else if(panStatus == 1){
        comingsoonBox.style.animation = "panin-r 2s cubic-bezier(.39,.58,.57,1) 0s 1 forwards";
        comingsoonText.textContent = "";
        panStatus = 0;
    }
})
