var audioPlayer = document.getElementsByClassName('audio-player')[0];
var showImg = document.getElementsByClassName('show-image');
var thumbnail = document.getElementById('thumb-id');
var coverImg = document.getElementById('cd-cover');
var gonogo = document.getElementsByClassName('play-button')[0];
var gonogoImg = document.getElementById('gonogo-btn');
var podFile = document.getElementById('pod-content');
var podText = document.getElementById('pod-name');
var redBar = document.getElementById('red-bar');
var whiteBar = document.getElementById('white-bar');
var closeAudioBtn = document.querySelector('.close-audio img');
var orientation = "";
var audioToPlay = ["https://cdn.the2minshow.ml/assets/audio/ep-audio/ep-1_nauru.mp3", "https://cdn.the2minshow.ml/assets/audio/ep-audio/ep-2_trinidadandtobago.mp3", "https://cdn.the2minshow.ml/assets/audio/ep-audio/ep-3_eritrea.mp3"]
const playImg = "/media/icons/play.svg";
const pauseImg = "/media/icons/pause.svg";
const replayImg = "https://img.icons8.com/color/48/000000/replay.png";
function checkOrientation(){
  if(window.innerWidth > window.innerHeight){orientation = "landscape"}
  else {orientation = "portrait"};
}
function showPlayer(i){
    audioPlayer.style.display = "block";
    var elementiq = document.getElementById("case" + i);
    var image = showImg[i].src;
    var text = showImg[i].alt;
    coverImg.src = image;
    podText.textContent = text;
    podFile.src = audioToPlay[i];
    podFile.play();
    if(orientation == "portrait"){elementiq.style.boxShadow = "0px 0px 7px 7px white";}
    else{elementiq.style.border = "2px solid white"};
    if(i>=3){alert('Coming Soon -- Czar')}
}

function gonogoChanger() {
    if (podFile.paused) {
        gonogoImg.src = playImg;
        podFile.play();
    } else {
        podFile.pause();
        gonogoImg.src = pauseImg;
    }
}
function thumbnailUpdate() {
    if (podFile.paused && podFile.currentTime < podFile.duration) { gonogoImg.src = playImg;}
    else if (podFile.paused && podFile.currentTime == podFile.duration) { gonogoImg.src = replayImg; showPlayer(i+1) }
    else { gonogoImg.src = pauseImg; }
}
function progressMaker() {
    whiteBar.style.width = (podFile.currentTime / podFile.duration) * 100 + "%";
    redBar.style.width = (podFile.currentTime / podFile.duration) * 100 + "%";
}
function closeAudio(){
    audioPlayer.style.display = "none";
    podFile.pause();
}
checkOrientation();
gonogo.addEventListener("click", gonogoChanger);
podFile.addEventListener("play", thumbnailUpdate);
podFile.addEventListener("pause", function(){thumbnailUpdate();});
podFile.addEventListener("timeupdate", progressMaker);
closeAudioBtn.addEventListener('click', closeAudio);
window.addEventListener('resize', checkOrientation);
